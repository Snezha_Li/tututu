const app = new Vue({
    el: '.row',
    data: {
        todos: [{
            name: 'Some name',
            description: 'Lorem ipsum',
            priority: 0,
            finished: false
        }],
        name: '',
        description: '',
        priority: '',
        editedIndex: null
    },
    methods: {
        createTodo: function(event){
            this.todos.unshift({
                name: this.name,
                description: this.description,
                priority: Number(this.priority),
                finished: false
            });
            this.name='';
            this.description='';
            this.priority = '0';

            event.preventDefault();
            console.log('create');
        },
        readTodo: function() {},
        updateTodo: function(index) {
            this.editedIndex = index;
            this.name = this.todos[index].name;
            this.description = this.todos[index].description;
            this.priority = this.todos[index].priority;
        },
        deleteTodo: function(index) {
            this.todos.splice(index, 1);
            this.refresh();
        },
    },

});
console.log(app);
